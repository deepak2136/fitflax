const permission = require("./permission.json");
const aes256 = require("./aes256");
const DBStatus = require("../util/database_constants");

module.exports.doAuthentication = function(req, res) {
  let url = req.url.toString();
  url = url.split("?")[0];
  if (permission["no-auth"].includes(url)) return true;

  let token = req.headers["authorization"];

  if (!token || token == undefined || token == null) {
    return "token not found.";
  }

  let tokenData;
  try {
    tokenData = aes256.decrypt(token);
  } catch (e) {
    return "token is not valid";
  }
  let jsonObj = JSON.parse(tokenData);
  let role = jsonObj["type"];

  console.log(jsonObj);
  let diff = Date.now() - jsonObj["created"];
  let days = diff / (1000 * 60 * 60 * 12);

  if (jsonObj["status"] != DBStatus.userStatus.ACTIVE || days > jsonObj["valid"]) {
    return "token expired. please login again.";
  }

  req.body.userTokenData = jsonObj;
  if (permission[role].includes(url)) {
    return true;
  } else {
    return "api permission denied.";
  }
};
