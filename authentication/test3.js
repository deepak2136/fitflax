const connection = require("../util/MySqlMaster");

f1 = () => {

    let sql = `SELECT _assigned_details.*, _goal.*, _user.* , _trainee.* 
	                FROM assign_user _assigned_details
	                JOIN goal _goal ON _assigned_details.gid = _goal.gid
	                JOIN trainee _trainee ON _trainee.tid = _assigned_details.tid
	                JOIN \`user\` _user ON _user.uid = _assigned_details.uid
                    WHERE _assigned_details.uid = 'kishan'`;

    let options = {
        sql: sql,
        nestTables: true
    }

    connection(options, [])
        .then((result) => {
            console.log(result['data'])
        }).catch((err) => {
            console.log(err);
        });
}

f2 = () => {
    connection('call updateConsultCount( ? )', [ 'dipak'])
        .then((result) => console.log(result))
        .catch((err) => console.log(err));
}

f3 = () => {
    connection("select get_image('14091523908205637','dipak') as image FROM DUAL", [])
    .then((result) => console.log(result))
    .catch((err) => console.log(err));
}

f4 = () => {
    connection("select addConsult('dipak', 12, 6) as status_deepak FROM DUAL", [])
    .then((result) => console.log(result['data']))
    .catch((err) => console.log(err));
}

f4();