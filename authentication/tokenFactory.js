const aes256 = require("./aes256");
const crypto = require("crypto");
const dateUtil = require("../util/dateUtil");

module.exports.getToken = function (data) {
  let type = data["type"].toString().toLowerCase();;
  let uid = data["uid"];

  let tokenData = {
    type: type,
    created: dateUtil.getCurrentDateTime(),
    valid: 90,
    status: "active",
    uid: uid
  };

  tokenData = shuffleProperties(tokenData);

  let token = aes256.encrypt(JSON.stringify(tokenData));
  return token;
};

/**
 * shuffle the properties of json object pass as an argument. it is uefull
 * to prevent attacks beacouse every time user request with same data it
 * show comlety redomized token value.
 */
Array.prototype.shuffle = function () {
  for (var i = 0; i < this.length; i++) {
    var a = this[i];
    var b = Math.floor(Math.random() * this.length);
    this[i] = this[b];
    this[b] = a;
  }
}

function shuffleProperties(obj) {
  var new_obj = {};
  var keys = getKeys(obj);
  keys.shuffle();
  for (var key in keys) {
    if (key == "shuffle") continue; // skip our prototype method
    new_obj[keys[key]] = obj[keys[key]];
  }
  return new_obj;
}

function getKeys(obj) {
  var arr = new Array();
  for (var key in obj)
    arr.push(key);
  return arr;
}