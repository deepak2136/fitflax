const crypto = require("crypto");

const secretKey = "fitflax token";

module.exports.encrypt = function encrypt(data) {
  let cipher = crypto.createCipher("aes-256-cbc", secretKey);
  let encrypted = cipher.update(data, "utf8", "hex");
  encrypted += cipher.final("hex");
  return encrypted.toString();
};

module.exports.decrypt = function decrypt(data) {
  let decipher = crypto.createDecipher("aes-256-cbc", secretKey);
  let decrypted = decipher.update(data, "hex", "utf8");
  decrypted += decipher.final("utf8");
  return decrypted;
};

module.exports.key = secretKey;
