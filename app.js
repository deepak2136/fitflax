var express = require("express");
var path = require("path");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");

var helmet = require("helmet");
var compression = require("compression");
var cors = require("cors");

var index = require("./routes/index");
var login = require("./routes/consult/login");
var authentication = require("./authentication/authentication");
var profile = require("./routes/profile/profile");
var user = require("./routes/user/user");

var goal = require("./routes/goal");
var workout = require("./routes/workout");
var plans = require("./routes/plans");
var offers = require("./routes/offers");

var consult_type = require("./routes/consult/consult_type");
var trainee = require("./routes/trainee/trainee");
var consult = require("./routes/consult/consult");
var feedback = require("./routes/feedback");

const order = require("./routes/order");
const payment_method = require("./routes/payment_methods");


var app = express();

/**
 * API that will serve data for any kind of client-side applications, we need to enable 
 * the CORS’s middleware for the endpoints become public. Meaning that some clients can 
 * make requests on our API.
 */
app.use(cors({
  methods: ["GET", "POST", "PUT", "DELETE"],
  allowedHeaders: ["Content-Type", "Authorization", "Accept"]
}));

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());

/**
 *  security middleware that handles several kinds of attacks in the HTTP/HTTPS protocols.
 */
app.use(helmet());

/**
 * To make requests lighter and load faster.responsible for compacting the JSON responses 
 * and also the static files which your application will serve to GZIP format, a compatible 
 * format to several browsers.
 */
app.use(compression());

/*
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods',
      'PUT, POST, PATCH, DELETE, GET ');
    return res.status(200).json({});
  }
});
*/

app.all("*", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");
  next();
});

app.all("*", function (req, res, next) {
  let status = authentication.doAuthentication(req, res);
  if (status == true) {
    console.log("getting true");
    next();
  } else {
    var error = {
      msg: status
    };
    res.status(200);
    res.json(error);
  }
});

app.use("/", index);
app.use("/login", login);
app.use("/profile", profile);
app.use("/user", user);

app.use("/goal", goal);
app.use("/workout", workout);
app.use("/plans", plans);
app.use("/offers", offers);

app.use("/consult_type", consult_type);
app.use("/trainee", trainee);
app.use("/consult", consult);
app.use("/feedback", feedback);

app.use("/order", order);
app.use("/payment/method", payment_method);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  console.log(err)
  try {
    res.status(err.status || 500);
    res.json({ msg: "api not found. please check documentation." });
  } catch (e) {
    res.json({ msg: "internal server error" });
  }
});











module.exports = app;
