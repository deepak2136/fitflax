var express = require("express");
var router = express.Router();

const connection = require("../../util/MySqlMaster");
const status = require("../../util/constants");
const httpResponse = require("../../util/HttpResponse");

const bcrypt = require("bcryptjs");
const randomstring = require("randomstring");
const email = require("../../util/email");

const profileTag = "_profile";
// var base64ToImage = require('base64-to-image');


router.get("/status", function (req, res, next) {

  let uid = req.body.userTokenData["uid"];
  var basicDetails;
  var pyschicalDetails;
  var goal;
  let sql =
    "select address, photo, contact, birthdate, gender from user where uid = ? and status = 'active'";
  let params = [uid];

  // check basic details of user.
  connection(sql, params)
    .then((result) => {
      let ress = result['data'][0];
      if (!ress || !ress["address"] || !ress["contact"] || !ress["photo"] || !ress["birthdate"] || !ress["gender"]) {
        basicDetails = "incomplete";
      } else {
        basicDetails = "complete";
      }

      // check physhical details of user.
      sql = "select * from physhical_details where uid = ?";
      connection(sql, params)
        .then((result1) => {

          let re = result1['data'][0];
          if (!re || !re["HEIGHT"] || !re["WEIGHT"] || !re["CAT"] || !re["DISABILITY"] || !re["REMARK"]) {
            pyschicalDetails = "incomplete";
          } else {
            pyschicalDetails = "complete";
          }

          // check user goal status whrther is set or not.
          sql = "select * from user_goal where uid = ? and status = 'active'";
          connection(sql, params)
            .then((result2) => {

              if (result2['data'].length > 0) {
                goal = "set";
              } else {
                goal = "panding";
              }

              var responseData = [{
                "basicDetails": basicDetails,
                "pyschicalDetails": pyschicalDetails,
                "goal": goal
              }];
              httpResponse.generateResponse(responseData, 200, status.SUCCESS, res);
            }).catch((error) => {
              console.error(error);
              httpResponse.generateResponse(null, 500, status.GOAL_NOT_SET, res);
            });//third query
        }).catch((error) => {
          console.error(error);
          httpResponse.generateResponse(null, 500, status.PHYSHICAL_DETAILS_NOT_FOUND, res);
        });//second query
    }).catch((error) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.USER_NOT_FOUND, res);
    });//first query
});

/**
 * upload profile image.
 * 
 * @param {base64} data image base64 string
 * @param {string} id image id to give as image name
 */
router.post("/upload/image", function (req, res, next) {

  let data = req.body.data;
  let id = req.body.userTokenData["uid"];
  let imageId = id + profileTag;

  let sql = 'select count(img.image_id) as count from image_store img where img.image_id = ?';
  let params = [imageId];

  connection(sql, params)
    .then((result) => {
      let count = parseInt(result['data'][0]['count']);
      console.log(result['data'])
      if (count === 0) {
        sql = 'insert into image_store values(?, ?, ?)';
        params = [imageId, data, id];

        connection(sql, params)
          .then((result) => {
            httpResponse.generateResponse(null, 200, status.UPDATE_SUCCESS, res);
          }).catch((err) => {
            console.log("check")
            console.error(error);
            httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
          });
      } else {
        sql = 'update image_store img set img.image_data = ? where image_id = ?';
        params = [data, imageId];

        connection(sql, params)
          .then((result) => {
            httpResponse.generateResponse(null, 200, status.UPDATE_SUCCESS, res);
          }).catch((err) => {
            console.log("check 1")
            console.error(error);
            httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
          });
      }

      sql = "update user u set u.photo = ? where uid = ? ";
      params = [imageId, id];

      connection(sql, params)
        .then((result) => {
          console.log('image id updated in user table');
        }).catch((error) => {
          console.log("check 2")
          console.error(error);
        });
    }).catch((err) => {
      console.log("check 3")
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * get your own profile picture. admin have permission to get other user's profile picture.
 * 
 * @param {string} id imageId or image name
 * @returns {base64} image data
 */
router.get("/get/image", function (req, res, next) {

  let uid = req.body.userTokenData["uid"];
  let imageId = req.query.id;

  if (imageId != null && imageId != undefined && !imageId && req.body.userTokenData["type"] == "admin") {
    imageId = req.query.id;
  } else {
    imageId = uid;
  }
  imageId = imageId + profileTag;

  let sql = "select image_data from image_store img where img.image_id = ? ";
  let params = [imageId];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((err) => {
      console.error(err);
      httpResponse.generateResponse(null, 404, status.IMAGE_NOT_FOUND, res);
    });
});

/**
 * @method get
 * 
 * this api is change user password to new temporary password. and new password send
 * it to given user's email addess.
 */
router.get("/forget/password", function (req, res, next) {

  let uid = req.query.id;
  let newPass = randomstring.generate({
    length: 12,
    charset: 'alphabetic'
  });
  console.log(newPass)
  let encryptPass = bcrypt.hashSync(newPass, 10);

  let sql = 'select u.name, u.email from user u where u.uid = ?';
  let params = [uid];

  connection(sql, params)
    .then((result) => {
      console.log(result);
      let emailTo = result['data'][0].email;
      let name = result['data'][0].name;

      email.sendmail(emailTo, encryptPass, uid);
    }).catch((err) => {
      console.error('email sending failed');
      console.error(err);
    });

  let sql1 = "update user set password=? where uid=?";
  let params1 = [encryptPass, uid];

  connection(sql1, params1)
    .then((result) => {
      if (result['data']["affectedRows"] > 0)
        httpResponse.generateResponse(null, 200, status.CHECK_MAIL_NEW_PASS, res);
      else
        httpResponse.generateResponse(null, 400, status.CONTACT_ADMIN, res);
    }).catch((err) => {
      console.error(err);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

module.exports = router;