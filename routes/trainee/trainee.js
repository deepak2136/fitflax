var express = require("express");
var router = express.Router();
var bcrypt = require("bcryptjs");
var nodemailer = require("nodemailer");
var smtpTransport = require('nodemailer-smtp-transport');


const connection = require("../../util/MySqlMaster");
const status = require("../../util/constants");
var httpResponse = require("../../util/HttpResponse");
const DBStatus = require("../../util/database_constants");
const dateUtil = require("../../util/dateUtil");
const queryExecutor = require("../../util/DBQueryService");
/**
 * 
 */

/* Email Client */
  
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
          user: 'barotrahulh123@gmail.com',
          pass: 'wmhvpaumfzyaoffv'
      }
    });

router.get("/goallist", function (req, res, next) {
  try {
    httpResponse.generateResponse(null, 500, status.NOT_IMPLEMENTED, res);
  } catch (err) {
    httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
  }
});

/**
 * assign trainee to given user.
 * 
 * @param {int} tid trainne id
 * @param {string} uid user id
 * @param {int} gid goal id
 * @param {string} desc description
 */
router.post("/assign/user", function (req, res, next) {

  let uid = req.body.uid;
  let tid = req.body.tid;
  let gid = req.body.gid;
  let desc = req.body.desc;

  if (!tid || !uid || !gid) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "select a.*, u.* from assign_user a, user u where a.uid = ? and u.uid = a.uid";
    let params = [uid];

    connection(sql, params)
      .then((result) => {
        if (result['data'].length <= 0) {
          sql = "select * from trainee where tid = ?";
          params = [tid];

          connection(sql, params)
            .then((result) => {
              if (result['data'].length >= 1) {
                sql = "insert into assign_user(TID, UID, GID, `DESC`) values(?, ?, ?, ?)";
                params = [tid, uid, gid, desc];

                connection(sql, params)
                  .then((result) => {
                    if (result['data']["affectedRows"] > 0)
                      httpResponse.generateResponse(null, 201, status.SUCCESS, res);
                    else
                      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
                  }).catch((err) => {
                    console.error(err);
                    httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
                  });// 3rd query
              } else {
                console.error("trainee not exists. check trainee id(tid).");
                httpResponse.generateResponse(null, 406, status.TRAINEE_NOT_EXISTS, res);
              }
            }).catch((err) => {
              console.error(err);
              httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });// 2nd query
        } else {
          console.error("user not exists. check user id(uid).");
          httpResponse.generateResponse(null, 406, status.ALREADY_TRAINEE_ASSIGNED, res);
        }
      }).catch((err) => {
        console.error(err);
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });//1st query
  }
});



/**
 * return all trainee details.
 * 
 * @param none
 */
router.get("/list", function (req, res, next) {

  let sql = "SELECT t.*, u.* FROM `trainee` t, `user` u WHERE t.`UID` = u.`UID`";
  let params = [];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * post trainne details.
 * 
 * @param {int} id trainee id
 */
router.post("/get", function (req, res, next) {

  let id = req.body.id;
  console.log(id)
  let sql = "SELECT t.*, u.* FROM `trainee` t, `user` u WHERE t.`UID` = u.`UID` and t.tid = ?";
  let params = [id];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
    });
});

/**
 * add new trainee. trainne must be register as a user first.
 * 
 * @param {string} uid usre id
 * @param {string} contact contact number
 * @param {string} avail_time_start availble time start 
 * @param {string} avail_time_end available time end
 * @param {int} gid goal id
 * @param {string} achivement achivement datils
 * @param {int} cat trainee crategory referance to consult_master
 */
router.post("/add", function (req, res, next) {

  let uid = req.body.uid;
  let contact = req.body.contact;
  let ats = req.body.avail_time_start;
  let ate = req.body.avail_time_end;
  let gid = req.body.gid;
  let achv = req.body.achivement;
  let type = req.body.cat;
  let email = req.body.email;
  let password = req.body.password;
  let user_cat = req.body.user_cat;

  if (!uid || !contact || !ats || !ate || !gid || !type) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {

    if (!uid || !email || !password || !user_cat) {
      httpResponse.generateResponse(null, 403, status.INPUT_INVALID, res);
    } else if (user_cat == DBStatus.userTypeId.ADMIN) {
      httpResponse.generateResponse(null, 403, status.CANNOT_ADD_ADMIN, res);
    } else {

      let pass = bcrypt.hashSync(password, 10);
      let sql = "insert into user(uid, email, password, catid, join_date, `status`) values(?, ?, ?, ?, ?, ?)";
      let currDate = dateUtil.getCurrentDateTime();
      let status = DBStatus.userStatus.ACTIVE;
      let params = [uid, email, pass, user_cat, currDate, status];

      connection(sql, params)
        .then((result) => {
    
          let sql = "insert into trainee(UID, CONTACT, AVAILABLE_TIME_START, AVAILABLE_TIME_END, GID, ACHIVEMENTS, TYPE) values(?, ?, ?, ?, ?, ?, ?)";
          let params = [uid, contact, ats, ate, gid, achv, type];

          connection(sql, params)
            .then((result) => {
              if (result['data']["affectedRows"] > 0) {

                sql = 'update `user` u set u.CATID = ? where u.UID = ?';
                params = [DBStatus.userTypeId.TRAINEE, uid];

                connection(sql, params)
                  .then((result) => {

                    const mailOptions = {
                    from: 'barotrahulh123@gmail.com', // sender address
                    to: email, // list of receivers
                    subject: 'Fitnflex Invitation', // Subject line
                    html: '<!DOCTYPE html> <html> <body> <p>Hello,</p><br><h3>Welcome to Fitnflex</h3> <p>your are Traineer at fitnflex you can access your account from below Credentail</p><br> <p>Username : '+email+'</p> <p>Password  : '+password+'</p> </body> </html>'// plain text body
                  };

                  transporter.sendMail(mailOptions, function (err, info) {
                     if(err)
                       console.log(err)
                     else
                       console.log("Mail Send");

                       // console.log(info);
                  });
                    console.log(uid + " user type changed to trainee");
                     httpResponse.generateResponse(null, 200, status.SUCCESS, res);

                  });
              }
            });

        }).catch((err) => {
          console.error(err);
          httpResponse.generateResponse(null, 402, status.ALREADY_EXISTS, res);
        });
    }
  }
});

/**
 * update existing trainee. trainne must be register as a user first.
 * 
 * @param {int} tid trainne id
 * @param {string} contact contact number
 * @param {string} avail_time_start availble time start 
 * @param {string} avail_time_end available time end
 * @param {int} gid goal id
 * @param {string} achivement achivement datils
 * @param {int} cat trainee crategory referance to consult_master
 */
router.put("/update", function (req, res, next) {

  let tid = req.body.tid;
  let contact = req.body.contact;
  let ats = req.body.avail_time_start;
  let ate = req.body.avail_time_end;
  let gid = req.body.gid;
  let achv = req.body.achivement;
  let type = req.body.cat;

  if (!contact || !ats || !ate || !gid || !type || !tid) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "update trainee t set t.contact = ?, t.available_time_start = ?, " +
      "t.available_time_end = ?, t.gid = ?, t.type = ?, t.achivements = ? where t.tid = ?";
    let params = [contact, ats, ate, gid, type, achv, tid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.UPDATE_SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.NOT_EXISTS, res);
      }).catch((error) => {
        console.log(error)
        httpResponse.generateResponse(null, 400, status.SERVER_ERROR, res);
      });
  }
});

/**
 * delete trainne.
 * 
 * @param {int} tid trainee id
 */
router.post("/delete", function (req, res, next) {
  let tid = req.body.tid;
  if (!tid) {
    httpResponse.generateResponse(null, 404, status.INPUT_MISSING, res);
  } else {
    let sql = "delete from trainee where tid = ?";
    let params = [tid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        console.error(error);
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * return the all tarinee for given goal.
 * 
 * @param {int} gid goal id.
 */
router.get("/goal", function (req, res, next) {
 
  let gid = req.query.gid;
  if (!gid) {
    httpResponse.generateResponse(null, 404, status.INPUT_MISSING, res);
  } else {
    let sql = "select * from trainee where gid = ?";
    let params = [gid];

    connection(sql, params)
      .then((result) => {
        httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
      }).catch((err) => {
        console.error(error);
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * return all user tarinee under the trainee.
 * admin can ask for users under for all trainner.
 * trainner have only rights to see user under her|his training.
 * 
 * @param {int} id trainee id. for admin only.
 */
router.get("/user/list", function (req, res, next) {

  if (req.query.id && req.body.userTokenData['type'] == 'admin') {
    let tid = req.query.id;
    this.getUserUnderTrainee(tid, res);
  } else {
    let uid = req.body.userTokenData['uid'];
    let sql = "select t.tid from trainee t where t.uid = ?";
    let params = [uid];

    connection(sql, params)
      .then((result) => {
        if (result['data'].length > 0) {
          let tid = parseInt(result['data'][0]['tid']);
          this.getUserUnderTrainee(tid, res);
        } else {
          console.log(status.NOT_TRAINEE);
          httpResponse.generateResponse(null, 406, status.NOT_TRAINEE, res);
        }
      }).catch((err) => {
        console.error(error);
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });// 1st query
  }
});

/**
 * get the user list under given trainee id;
 * 
 * @param {int} tid trainee id
 * @param {response} res response object to send response.
 * 
 * @returns {array users} list of users.
 */
getUserUnderTrainee = function (tid, res) {
  let sql = "SELECT u.*, pd.* " +
    "FROM `user` u " +
    "INNER JOIN assign_user au ON u.uid = au.uid " +
    "INNER JOIN physhical_details pd ON pd.uid = u.uid " +
    "WHERE au.tid = ? ";
  let params = [tid];
      console.log(sql);

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((err) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
};

/**
 * trainee only.
 * get all panding requests of users. if i'm trainee then i can get all request of
 * user assined to me.
 * 
 * Admin can also get panding requests for specified trainne. you just need to
 * pass id of traine at the end of URL.
 * example: /panding/requests?id=userID of trainne
 */
router.get("/panding/requests", function (req, res, next) {

  let uid = req.body.userTokenData["uid"];

  if(req.body.userTokenData["type"] == "admin") {
    if(req.query.id)
       uid = req.query.id;
    else 
      httpResponse.generateResponse(null, 500, status.NOT_TRAINEE, res);
  } 

  let sql = `SELECT _request_diet.asid,_request_diet.request_date, _request_diet.counter as request_count, _request_diet.status, _request_diet.height, _request_diet.weight, 
            _request_diet.remark, image.image_id, image.image_data, _counsult_type.name,  _counsult_type.desc
            FROM diet _request_diet
            JOIN consult_master _counsult_type
            ON _request_diet.type = _counsult_type.id
            LEFT JOIN image_store image
            ON image.image_id = _request_diet.photo
            WHERE _request_diet.status = ? AND _request_diet.asid IN
            (SELECT au.id 
            FROM assign_user au 
            WHERE au.tid =?) `;
  let params = [DBStatus.dietStatus.REQUESTED, uid];
  let options = {
    sql: sql,
    // nestTables: true
  }

  connection(options, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((err) => {
      console.log(err);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

module.exports = router;