var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
const status = require("../util/constants");
var httpResponse = require("../util/HttpResponse");

/**
 * this method return all the plans.
 * 
 * @param none
 * @returns list of all plans.
 */
router.get("/list", function (req, res, next) {

  let sql = "select p.*, c.* from plans p, consult_master c where p.type = c.id";
  let params = [];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * return the details of plan for given plan id.
 * 
 * @param {int} pid plan id.
 * @returns plan deatails
 */
router.get("/get", function (req, res, next) {

  let pid = req.query.id;
  let sql = "select p.*, c.* from plans p, consult_master c where p.type = c.id and  pid = ?";
  let params = [pid];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
    });
});

/**
 * create new record for given input;
 * 
 * @param {int} pid plan id.
 * @param {string} name plan name.
 * @param {string} details plan details.
 * @param {float} fees plan fees in ruppes.
 * @param {string} duretion example 1W = one week 1M = one month
 * @param {int} type plan type
 * @param {int} consult_count consult include in plan.
 */
router.post("/add", function (req, res, next) {

  let name = req.body.name;
  let details = req.body.details;
  let fees = req.body.fees;
  let duretion = req.body.duretion;
  let type = req.body.cat;
  let cc = req.body.consult_count;

  if (!name || !details || !fees || !duretion || !type || !cc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "insert into plans(NAME, DETAILS, FEES, DURETION, CONSULT_COUNT, TYPE) values(?, ?, ?, ?, ?, ?)";
    let params = [name, details, fees, duretion, cc, type];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      }).catch((error) => {
        console.log(error)
        httpResponse.generateResponse(null, 400, status.ALREADY_EXISTS, res);
      });
  }
});

/**
 * update plan details for given plan id;
 * 
 * @param {int} pid plan id.
 * @param {string} name plan name.
 * @param {string} details plan details.
 * @param {float} fees plan fees in ruppes.
 * @param {string} duretion example 1W = one week 1M = one month
 * @param {int} type plan type
 * @param {int} consult_count consult include in plan.
 */
router.put("/update", function (req, res, next) {

  let pid = req.body.id;
  let name = req.body.name;
  let details = req.body.details;
  let fees = req.body.fees;
  let duretion = req.body.duretion;
  let type = req.body.cat;
  let cc = req.body.consult_count;

  if (!name || !details || !fees || !duretion || !type || !cc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "update plans p set p.name = ?, p.details = ?, p.fees = ?, p.duretion = ?, p.type = ?, " +
      " p.consult_count = ? where p.pid = ?";
    let params = [name, details, fees, duretion, type, cc, pid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.UPDATE_SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        console.log(error)
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * delete plan for given plan id.
 * 
 * @param {int} pid plan id.
 */
router.delete("/delete", function (req, res, next) {

  let pid = req.body.id;
  if (!pid) {
    httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
  } else {
    let sql = "delete from plans where pid = ?";
    let params = [pid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

module.exports = router;