var express = require("express");
var router = express.Router();

const connection = require("../../util/MySqlMaster");
const status = require("../../util/constants");
var httpResponse = require("../../util/HttpResponse");
var bcrypt = require("bcryptjs");

const DBConst = require("../../util/database_constants");
const dateUtil = require("../../util/dateUtil");
const queryExecutor = require("../../util/DBQueryService");
// var common_fun = require('../../common_fun.js');

/**
 * get unassigned user list. unassigned means user haven't assign a trainee.
 * @param none
 */
router.get("/unassigned", function (req, res, next) {
  /*
      sql query
      desc:- gives list of user who havent assignd to trainee

      select distinct u.uid, u.password, g.name as goal, u.email, ug.start_date
      from user u
      join user_goal as ug on u.uid = ug.uid
      join goal as g on g.gid = ug.gid
      join user_cat as uc on u.catid = uc.cat_id
      where u.uid not in ( select uid from sql12212461.user_assign )  
      and u.status = 'active'
      and uc.name = 'user'

  */

  let sql =
    "select distinct u.uid, u.contact, g.name as goal, u.email, ug.start_date " +
    " from user u " +
    " left join user_goal as ug on u.uid = ug.uid " +
    " left join .goal as g on g.gid = ug.gid " +
    " left join user_cat as uc on u.catid = uc.cat_id " +
    " where u.uid not in ( select uid from assign_user ) " +
    " and u.status = 'active' " +
    " and uc.name = 'user' ";
  let params = [];

  connection(sql, params)
    .then((result) => {
      if (result['data'].length > 0)
        httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
      else
        httpResponse.generateResponse(null, 444, status.NO_RECORDS_FOUND, res);
    }).catch((error) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    })
});

/**
 * set user goal.
 * 
 * @param {string} uid user ID
 * @param {int} gid goal ID
 * @param {string} status goal status active | inactive | complete.
 */
router.post("/setgoal", function (req, res, next) {

  let uid = req.body.uid;
  let gid = req.body.gid;
  let _status = req.body.status;

  if (!uid || !gid) {
    httpResponse.generateResponse(null, 400, status.INPUT_MISSING, res);
  } else {
    if (!_status) {
      _status = DBConst.goal_status.ACTIVE;
    }

    let sql = "insert into user_goal(uid, gid, start_date, status) values(?, ?, ?, ?)";
    let params = [uid, gid, new Date(Date.now()).toLocaleString(), _status];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.INSERT_FAILED, res);
      }).catch((error) => {
        console.error(error);
        httpResponse.generateResponse(null, 406, status.GOAL_ALREADY_REQUESTED, res);
      });
  }
});

/**
 * get all user datils. only admin have authority get all users data. trainee have permission
 * to get user data whose trained by him|her.
 * 
 * @param none
 */
router.get("/getallusers", function (req, res, next) {

  let sql =
    "SELECT u.uid, u.name, u.email, u.address, u.contact, u.photo, u.birthdate, uc.name as 'type', " +
    " u.catid as 'user_cat_id' " +
    " from user u " +
    " left join user_cat as uc on uc.cat_id= u.catid " +
    " where u.status = 'active' ";
  let params = [];

  connection(sql, params)
    .then((result) => {
      if (result['data'].length > 0)
        httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
      else
        httpResponse.generateResponse(null, 400, status.NO_RECORDS_FOUND, res);
    }).catch((error) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * get all user datils. only admin have authority get all users data. trainee have permission
 * to get user data whose trained by him|her.
 * 
 * @param none
 */
router.get("/unassigned/users", function (req, res, next) {

  let sql =
    "select distinct u.uid,u.name, u.contact, g.name as goal, u.email, ug.start_date " +
    " from user u " +
    " left join user_goal as ug on u.uid = ug.uid " +
    " left join .goal as g on g.gid = ug.gid " +
    " left join user_cat as uc on u.catid = uc.cat_id " +
    " where u.uid not in ( select uid from assign_user ) " +
    " and u.status = 'active' " +
    " and uc.name = 'user' ";
  let params = [];

  connection(sql, params)
    .then((result) => {
      if (result['data'].length > 0)
        httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
      else
        httpResponse.generateResponse(null, 444, status.NO_RECORDS_FOUND, res);
    }).catch((error) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    })
});

/** ************ pnading ************** */

/**
 * get all details of user.
 * if user type is admin then it can pass id of user in url and also get details of that user.
 * it gives admin to access all information of user.
 * 
 * @param {string} uid user id (only for admin).
 */
router.get("/getprofile", function (req, res, next) {

  let uid;
  let response = {};

  if (req.body.userTokenData["type"] == "admin" && req.query.id) {
    uid = req.query.id;
  } else {
    uid = req.body.userTokenData["uid"];
  }

  let sql =
    "SELECT u.uid, u.name, u.email, u.address, u.contact, u.photo, u.birthdate, uc.name as 'type', " +
    " u.catid as 'user_cat_id', u.password, u.gender,pd.HEIGHT,pd.WEIGHT ,(SELECT COUNT(GID) FROM user_goal WHERE uid = u.UID  AND STATUS != 'complete') as goal_status,(SELECT COUNT(UID) FROM assign_user WHERE uid = u.UID ) as diet_status from user u " +
    " left join user_cat as uc on uc.cat_id= u.catid " +
    " left join physhical_details as pd on u.UID= pd.UID " +
    " where u.status = 'active' and u.uid = ?";
  let params = [uid];

  connection(sql, params)
    .then((result) => {
      if (result['data'].length > 0) {

        result['data'][0]["profile_status"] = (result['data'][0]["HEIGHT"] != null && result['data'][0]["WEIGHT"] != null)?'1':'0';
        response.basicDetaisl = result['data'][0];
      } else {
        response.basicDetaisl = null;
      }

      sql = "select * from physhical_details where uid = ?";
      connection(sql, params)
        .then((result1) => {
          if (result1['data'].length > 0) {
            response.pyschicalDetaisl = result1['data'][0];
          } else {
            response.pyschicalDetaisl = null;
          }

          sql = "SELECT g.name as 'goal', ug.start_date, ug.end_date, ug.status " +
            " FROM user_goal ug " +
            " join goal as g on ug.gid = g.gid " +
            " where ug.uid = ?";

          connection(sql, params)
            .then((result2) => {
              if (result2['data'].length > 0) {
                response.goalDetais = result2['data'];
              } else {
                response.goalDetais = null;
              }
              console.log(response);

              httpResponse.generateResponse(response, 200, status.SUCCESS, res);
            }).catch((error) => {
              console.error(error);
              httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });//3rd query
        }).catch((error) => {
          console.error(error);
          httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
        });//2nd query
    }).catch((error) => {
      console.error(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });//1st query
});

/**
 * update physchical details of user.
 * user can update only his/ her datails.
 * if user is admin and he has right to update physchical details of his/ her selft as well as any user
 * 
 * @param {string} height
 * @param {string} weight
 * @param {string} category Ex. veg | non-veg | jain
 * @param {string} disablility
 * @param {string} remark
 * @param {string} uuid (only for admin).
 */
router.put("/update/physhical_details", function (req, res, next) {

  let height = req.body.height;
  let weight = req.body.weight;
  let cat = req.body.cat;
  let disability = req.body.disability;
  let remark = req.body.remark;
  let uid;

  if (req.body.userTokenData["type"] == "admin" && req.body.uid) {
    uid = req.body.uid;
  } else {
    uid = req.body.userTokenData["uid"];
  }

  let sql = "update physhical_details p set p.height = ?, p.weight = ?, p.cat = ?, p.remark = ?, " +
    " p.disability = ? where uid = ?";
  let params = [height, weight, cat, remark, disability, uid];

  connection(sql, params)
    .then((result) => {
      if (result['data']["affectedRows"] > 0)
        httpResponse.generateResponse(null, 200, status.SUCCESS, res);
      else
        httpResponse.generateResponse(null, 400, status.UPDATE_FAILED, res);
    }).catch((er) => {
      console.error(er);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * update basic details of user.
 * admin have rights to update basic details of user.
 * 
 * @param {string} name
 * @param {string} address
 * @param {string} contact
 * @param {string} birthdate
 * @param {string} gender Ex. M | F
 * @param {string} uid (for admin only)
 */
router.put("/update/basic_details", function (req, res, next) {

  let name = req.body.name;
  let address = req.body.address;
  let contact = req.body.contact;
  let birthdate = req.body.birthdate;
  let gender = req.body.gender;
  let uid;

  if (req.body.userTokenData["type"] == "admin" && req.body.uid) {
    uid = req.body.uid;
  } else {
    uid = req.body.userTokenData["uid"];
  }

  let sql = "update user u set u.name = ?, u.address = ?, u.contact = ?, u.birthdate = ?," +
    " u.gender = ? where uid = ?";
  let params = [name, address, contact, birthdate, gender, uid];

  connection(sql, params)
    .then((result) => {



      if (result['data']["affectedRows"] > 0)
      {
          var at=[];
          var notifi_message=[];
          // notifi_message.push({message:'Basic details updated',tag:'9'});
                              
          // at.push('dKdSb0yFHSo:APA91bHF6q3zTh9Kw6POGNou-D8KCgCm1VRfC39z_rFfEG_cC_w_IoKCS3YyRGS8IcX-yTKfCFuMiUlcij-4X6dos4gBsfK8V1xI0338H18mYrlwoZZCyzZUw2D24KQZW0DI6WbeJeYO'); 
          // common_fun.android(at,notifi_message);
        httpResponse.generateResponse(null, 200, status.UPDATE_SUCCESS, res);
      }
      else{
        httpResponse.generateResponse(null, 400, status.UPDATE_FAILED, res);
      }
    }).catch((er) => {
      console.error(er);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * change user password. admin have righs to change any other user's password.
 * 
 * @param {string} oldpassword
 * @param {String} newpassword
 * @param {string} uuid (for admin only)
 */
router.put("/change/password", function (req, res, next) {

  let uid;
  let oldPassword = req.body.oldpassword;
  let newPassword = req.body.newpassword;

  if (req.body.userTokenData["type"] == "admin" && req.body.uid) {
    uid = req.body.uid;
  } else {
    uid = req.body.userTokenData["uid"];
  }

  let sql = "select password from user where uid=?";
  let params = [uid];

  connection(sql, params)
    .then((result) => {
      if (result['data'] != null) {
        let pass = result['data'][0]['password'];

        if (bcrypt.compareSync(oldPassword, pass)) {
          let encryptNewPassword = bcrypt.hashSync(newPassword, 10);
          sql = "update user set password=? where uid=?";
          params = [encryptNewPassword, uid];

          connection(sql, params)
            .then((result) => {
              if (result['data']["affectedRows"] > 0)
                httpResponse.generateResponse(null, 200, status.UPDATE_SUCCESS, res);
              else
                httpResponse.generateResponse(null, 400, status.UPDATE_FAILED, res);
            }).catch((err) => {
              console.error(err);
              httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });//2nd query
        } else {
          httpResponse.generateResponse(null, 403, status.PASS_NOT_MATCH, res);
        }//inner if

      } else {
        httpResponse.generateResponse(null, 400, status.ALREADY_EXISTS, res);
      }//outer if
    }).catch((err) => {
      console.error(err);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    })//1st query
});

/**
 * This is a api that create new user. ony admin have right to add new user.
 * Admin can't add user as a admin.
 * 
 * @param {string} uid
 * @param {string} password
 * @param {string} email
 * @param {int} user_cat user category and sould not be admin
 */
router.post("/new", function (req, res, next) {

  let uid = req.body.uid;
  let email = req.body.email;
  let password = req.body.password;
  let user_cat = req.body.user_cat;

  if (!uid || !email || !password || !user_cat) {
    httpResponse.generateResponse(null, 403, status.INPUT_INVALID, res);
  } else if (user_cat == DBConst.userTypeId.ADMIN) {
    httpResponse.generateResponse(null, 403, status.CANNOT_ADD_ADMIN, res);
  } else {
    let pass = bcrypt.hashSync(password, 10);
    let sql = "insert into user(uid, email, password, catid, join_date, `status`) values(?, ?, ?, ?, ?, ?)";
    let currDate = dateUtil.getCurrentDateTime();
    let status = DBConst.userStatus.ACTIVE;
    let params = [uid, email, pass, user_cat, currDate, status];

    connection(sql, params)
      .then((result) => {
        httpResponse.generateResponse(null, 200, status.SUCCESS, res);
      }).catch((err) => {
        console.error(err);
        httpResponse.generateResponse(null, 402, status.ALREADY_EXISTS, res);
      });
  }
});

/**
 * Permission only for admin. admin can change user goal status to
 * requested to active or any thing. default status set to active
 * 
 * @param uid Id of user
 * @param state Ststus you wannna set
 * @param gid goalID
 */
router.post("/goal/status/update", async function (req, res, next) {

  let uid = req.body.uid;
  let state = req.body.state;
  let gid = req.body.gid;

  if( status == null || !status )
    this.state = DBConst.goal_status.ACTIVE;

  if(!uid || !gid) {
    httpResponse.generateResponse(null, 403, status.INPUT_INVALID, res);
  }

  let sql = "select * from user_view where uid = ?";
  let params = [uid];

  let userData = await queryExecutor(sql, params, res);
  if( userData.data.length <= 0 )
    httpResponse.generateResponse(null, 404, status.USER_NOT_EXISTS, res);

  sql = "update user_goal set status = ? where uid = ? and gid = ?";
  params = [state, uid, gid];

  await queryExecutor(sql, params, res);
  httpResponse.generateResponse(null, 200, status.SUCCESS, res);
});




router.post("/update/token", function (req, res, next) {

  let sql = "update user SET token = ? where uid = ?";
  let params = [req.body.token, req.body.uid];
  connection(sql, params)
    .then((result) => {
      if (result['data']["affectedRows"] > 0)
        httpResponse.generateResponse(null, 200, status.SUCCESS, res);
      else
        httpResponse.generateResponse(null, 400, status.UPDATE_FAILED, res);
    }).catch((er) => {
      console.error(er);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

module.exports = router;