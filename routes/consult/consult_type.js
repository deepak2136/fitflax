var express = require("express");
var router = express.Router();

const connection = require("../../util/MySqlMaster");
var httpResponse = require("../../util/HttpResponse");
const status = require("../../util/constants");

/**
 * get the all consult types available.
 */
router.get("/list", function (req, res, next) {

    let sql = "select * from consult_master";
    let params = [];

    connection(sql, params)
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
        }).catch((error) => {
            console.error(error);
            httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
        });
});

/**
 * get the consult details for specified id.
 * 
 * @param {int} id id of consult type
 */
router.get("/get", function (req, res, next) {

    let id = req.query.id;
    let sql = "select * from consult_master where id =?";
    let params = [id];

    connection(sql, params)
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
        }).catch((error) => {
            console.log(error);
            httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
        });
});

/**
 * add the consult type.
 * 
 * @param {string} name name of consult name
 * @param {string} desc description of cunsult
 */
router.post("/add", function (req, res, next) {

    let name = req.body.name;
    let desc = req.body.desc;

    if (!name || !desc) {
        httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
    } else {
        let sql = "insert into consult_master(`name`,`desc`) values(?, ?)";
        let params = [name, desc];

        connection(sql, params)
            .then((result) => {
                if (result['data']["affectedRows"] > 0) {
                    let resData = { id: result['data'.insertId] };
                    httpResponse.generateResponse(resData, 201, status.SUCCESS, res);
                }
                else
                    httpResponse.generateResponse(null, 500, status.ALREADY_EXISTS, res);
            }).catch((error) => {
                console.error(error);
                httpResponse.generateResponse(null, 400, status.SERVER_ERROR, res);
            });
    }
});

/**
 * update the consult type.
 * 
 * @param {int} id consult type id
 * @param {string} name name of consult name
 * @param {string} desc description of cunsult
 */
router.put("/update", function (req, res, next) {

    let id = req.body.id;
    let name = req.body.name;
    let desc = req.body.desc;

    if (!id || !name || !desc) {
        httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
    } else {
        let sql = "update consult_master c set c.name = ?, c.desc = ? where c.id = ?";
        let params = [name, desc, id];

        connection(sql, params)
            .then((result) => {
                if (result['data']["changedRows"] > 0)
                    httpResponse.generateResponse(null, 201, status.SUCCESS, res);
                else
                    httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
            }).catch((error) => {
                console.error(error);
                httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });
    }
});

/**
 * detele the consult type.
 * 
 * @param {int} id consult id
 */
router.delete("/delete", function (req, res, next) {

    let id = req.body.id;
    if (!id) {
        httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
    } else {
        let sql = "delete from consult_master where id = ?";
        let params = [id];

        connection(sql, params)
            .then((result) => {
                if (result['data']["affectedRows"] > 0)
                    httpResponse.generateResponse(null, 200, status.SUCCESS, res);
                else
                    httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
            }).catch((error) => {
                console.error(error);
                httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });
    }
});

module.exports = router;