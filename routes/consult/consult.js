var express = require("express");
var router = express.Router();

const connection = require("../../util/MySqlMaster");
var httpResponse = require("../../util/HttpResponse");

const responseStatus = require("../../util/constants");
const status = require("../../util/constants");
const DBStatus = require("../../util/database_constants");

const dateUtil = require("../../util/dateUtil");

/**
 * this web service is invoked when user request for diet.
 * 
 * @param {int} asid assign users's id
 * @param {int} type consult type
 * @param {base64} photo [optional] photo of user of current day 
 * @param {string} duretion [optional] diet duretion
 * @param {string} height 
 * @param {string} weight
 * @param {string} remark [optional] user remarks to consider for diet.
 */
router.post("/request/diet", function (req, res, next) {

    let asid = req.body.asid;
    let type = req.body.type;
    let photo = req.body.photo;
    let duretion = req.body.duretion;

    let height = req.body.height;
    let weight = req.body.weight;
    let remark = req.body.remark;
    let uid = req.body.userTokenData["uid"];

    if (!duretion)
        duretion = DBStatus.duretion.W1;

    if (!asid || !type || !height || !weight) {
        httpResponse.generateResponse(null, 405, responseStatus.INPUT_MISSING, res);
    } else {

        let sql = "select * from assign_user u where u.id = ?";
        let params = [asid];

        connection(sql, params)
            .then((result) => {
                if (result['data'].length > 0) {

                    let imageId = "";
                    if (photo) {
                        imageId = uid + new Date().getTime();

                        sql = 'insert into image_store values(?, ?, ?)';
                        params = [imageId, photo, uid];

                        connection(sql, params)
                            .then((result) => {
                                console.log('diet image stored');
                            }).catch((err) => {
                                console.log('diet image storing failed');
                            });
                    }

                    sql = "select count(*) as count from diet where asid = ?";
                    params = [asid];
                    let totalDiet = 1;

                    connection(sql, params)
                        .then((result) => {
                            totalDiet += result['data'][0]['count'];
                            params = [asid, type, dateUtil.getCurrentDateTime(), imageId, totalDiet, DBStatus.dietStatus.REQUESTED, height, weight, remark];
                            askForDiet(params, res);
                        }).catch((err) => {
                            totalDiet = 1;
                            params = [asid, type, dateUtil.getCurrentDateTime(), imageId, totalDiet, DBStatus.dietStatus.REQUESTED, height, weight, remark];
                            askForDiet(params, res);
                        })
                } else {
                    httpResponse.generateResponse(null, 406, responseStatus.NOT_ASSIGNED, res);
                }
            }).catch((err) => {
                console.error(err);
                httpResponse.generateResponse(null, 404, responseStatus.SERVER_ERROR, res);
            });// 1st query
    }
});

askForDiet = function (params, res) {
    let sql = "insert into diet(asid, `type`, request_date, photo, counter, `status`, height, weight, remark) " +
        "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    connection(sql, params)
        .then((result) => {
            httpResponse.generateResponse(null, 200, responseStatus.SUCCESS, res);
        }).catch((err) => {
            console.error(err);
            httpResponse.generateResponse(null, 404, responseStatus.SERVER_ERROR, res);
        });
};

/**
 * this method gives all details for assigned trainee to user. it gives details of
 * trainee assigned to user, goals of user, it own user basic details as well as 
 * assined details.
 * 
 * @param none
 * @method get
 */
router.get("/assigned/details", function (req, res, next) {

    let uid;
    if (req.body.userTokenData["type"] == "admin") {
      
        uid = req.query.id;

    }
    else
    {

        uid = req.body.userTokenData["uid"];
    }

    let sql = `SELECT _assigned_details.*, _goal.*, _user.* , _trainee.* 
	                FROM assign_user _assigned_details
	                JOIN goal _goal ON _assigned_details.gid = _goal.gid
	                JOIN trainee _trainee ON _trainee.tid = _assigned_details.tid
	                JOIN \`user\` _user ON _user.uid = _assigned_details.uid
                    WHERE _assigned_details.uid = ?`;
    let params = [uid];
    let options = {
        sql: sql,
        nestTables: true
    }

    connection(options, params)
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, responseStatus.SUCCESS, res);
        }).catch((err) => {
            console.error(err);
            httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
        });
});

/**
 * this method assign user a diet or workout.
 * 
 * @param {int} asid assign_user_id
 * @param {JSON} workout wourkout detils
 * @param {JSON} diet diet details
 * 
 */


 /**
     * check asid and user is valid
     * assign diet
     * see valid code count
     * chnage diet status
     * reduce code count
     */
router.post("/diet", function (req, res, next) {
    
    let sql = "update diet set workout = ?, assign_date = ?, duretion = ?, status = ?, remark = ?,diet = ? where asid = ?";
    let params = [req.body.wid,dateUtil.getCurrentDateTime(),req.body.duretion,DBStatus.dietStatus.ASSIGNED,req.body.remark,req.body.diet,req.body.asid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.NOT_EXISTS, res);
      }).catch((error) => {
        console.log(error)
        httpResponse.generateResponse(null, 400, status.SERVER_ERROR, res);
      });
});

/**
 * get all panding request for trainee.
 * @param none
 * @returns list of panding requests.
 */
router.post("/panding", function (req, res, next) {

    // let uid = req.body.userTokenData["uid"];
    let uid = req.body.tid;


    let sql = `SELECT u.name,u.contact,d.asid,d.did AS deit_id, cm.name AS counsult_type, d.request_date, img.image_data AS image, d.counter AS diet_number,
    d.status AS STATUS, d.height AS height, d.weight AS weight, d.remark AS remark 
    FROM diet d
    LEFT OUTER JOIN image_store img ON img.image_id = d.photo
    LEFT OUTER JOIN consult_master cm ON cm.id = d.type
    LEFT JOIN assign_user au1 ON au1.id = d.asid
    LEFT JOIN user u ON u.uid = au1.uid
    WHERE  d.status = ? AND d.asid IN
    (SELECT au.id FROM assign_user au WHERE au.tid IN
    (SELECT t.tid FROM trainee t WHERE t.uid = ?))
    ORDER BY d.request_date DESC`;
    let params = [DBStatus.dietStatus.REQUESTED, uid];

    connection(sql, params)
        .then((result) => {
            httpResponse.generateResponse(result.data, 200, responseStatus.SUCCESS, res);
        }).catch((err) => {
            console.log(err);
            httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
        });
});

module.exports = router;