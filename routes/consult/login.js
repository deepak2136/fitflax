var express = require("express");
var router = express.Router();

const connection = require("../../util/MySqlMaster");
const tokenFactory = require("../../authentication/tokenFactory");

var bcrypt = require("bcryptjs");
var httpResponse = require("../../util/HttpResponse");
const status = require("../../util/constants");

const dateUtil = require("../../util/dateUtil");
const DBConstants = require("../../util/database_constants");
const validator = require("../../util/validation");

/**
 * return full details of passed user id.
 * 
 * @param { String } userID don't need to pass, it set by authntication process
 * @returns full details of user.
 */
router.get("/getuserdetails", function (req, res, next) {

  let userId = req.query.id;
  let query = "select * from user_view where UID=?";
  let params = [userId];

  connection(query, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.error(error);
      httpResponse.sendResponse(null, 404, status.SERVER_ERROR, res);
    });
});


/**
 * this method is to check login. if login detials is valid then it return
 * access token to access other api.
 *  
 * @param email email id or user name both passed. check email using validator.
 * @param pass password
 */
router.post("/login", async function (req, res, next) {
  let pass = req.body.pass;
  let email = req.body.email;

  if (!pass || !email) {
    httpResponse.generateResponse(null, 401, status.AUTH_FAIL, res);
  } else {
    let sql;
    let params = [];
    if (validator.isValidEmail(email) === false) {

      sql =
        "select u.uid, u.name, c.NAME as type, u.PASSWORD as password, u.photo, u.email, u.address, u.contact,pd.HEIGHT as height,pd.WEIGHT as weight,(SELECT COUNT(GID) FROM user_goal WHERE uid = u.UID  AND STATUS != 'complete') as user_goal,(SELECT COUNT(UID) FROM assign_user WHERE uid = u.UID ) as diet_goal from user u, " +
        "user_cat c,physhical_details pd where u.UID = ? and u.CATID = c.CAT_ID and u.status = ? and pd.UID =u.UID";
      params = [email, DBConstants.userStatus.ACTIVE];

    } else {
      sql =
        "select u.uid, u.name, c.NAME as type, u.PASSWORD as password, u.photo, u.email, u.address, u.contact,pd.HEIGHT as height,pd.WEIGHT as weight,(SELECT COUNT(GID) FROM user_goal WHERE uid = u.UID AND STATUS != 'complete') as user_goal,(SELECT COUNT(UID) FROM assign_user WHERE uid = u.UID ) as diet_goal from user u, " +
        "user_cat c,physhical_details pd where u.email = ? and u.CATID = c.CAT_ID and u.status = ? and pd.UID =u.UID";
      params = [email, DBConstants.userStatus.ACTIVE];
    }

    params.push(pass);
    let logingData = await executeQuery(sql, params, res);

    if (logingData['data'].length <= 0) {
      console.log("Invalid login credentials");
      httpResponse.generateResponse(null, 401, status.INVALID_LOGIN, res);
    }

    if (bcrypt.compareSync(pass, logingData['data'][0]["password"])) {
      let token = tokenFactory.getToken(logingData['data'][0]);
      logingData['data'][0].token = token;
      logingData['data'][0]["password"] = "**********";
     
       logingData['data'][0]["profile_status"] = (logingData['data'][0]["height"] != null && logingData['data'][0]["weight"] != null)?'1':'0';
       logingData['data'][0]["goal_status"]  = logingData['data'][0]["user_goal"] > 0?'1':'0';
       logingData['data'][0]["diet_status"]  = logingData['data'][0]["diet_goal"] > 0?'1':'0';



      console.log("login successfully.")
      let responseData = [logingData['data'][0]];
  
      httpResponse.generateResponse(responseData, 200, status.SUCCESS, res);
    } else {
      console.log("invalid password.");
      httpResponse.generateResponse(null, 403, status.INVALID_LOGIN, res);
    }
  }
});

executeQuery = async (sql, params, res) => {
  try {
    return await connection(sql, params);
  } catch (err) {
    console.log(err);
    httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
  }
}

/**
 * create a new user with given parameters. when user record created successfully then
 * it return access token to access other api. 
 * 
 * @param {string} uid user id
 * @param {string} pass password
 * @param {string} name name
 * @param {character} gen gender
 * @param {string} contact contact number 
 */
router.post("/signup", function (req, res, next) {
  let uid = req.body.uid;
  let pass = req.body.pass;
  let name = req.body.name;
  let email = req.body.email;
  let cat = DBConstants.userTypeId.USER;
  let gen = req.body.gender;
  let contact = req.body.contact_no;
  let status = DBConstants.userStatus.ACTIVE;
  let currentDateTime = dateUtil.getCurrentDateTime();

  if (pass == null || name == null || uid == null || email == null) {
    console.log("invalid parameters");
    httpResponse.generateResponse(null, 403, status.INPUT_INVALID, res);
  } else {
    pass = bcrypt.hashSync(pass, 10);

    let sql =
      "insert into user(UID, PASSWORD, NAME, EMAIL, CATID, STATUS, GENDER, CONTACT, JOIN_DATE) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    let params = [uid, pass, name, email, cat, status, gen, contact, currentDateTime];

    connection(sql, params)
      .then((result) => {
        sql = "insert into physhical_details(uid) values (?)";
        params = [uid + ""];

        connection(sql, params)
          .then((result) => {
            sql =
              "select u.uid, u.name, c.NAME as type, u.PASSWORD as password, u.photo, u.email, u.address, u.contact,pd.HEIGHT as height,pd.WEIGHT as weight,(SELECT COUNT(GID) FROM user_goal WHERE uid = u.UID  AND STATUS != 'complete') as user_goal,(SELECT COUNT(UID) FROM assign_user WHERE uid = u.UID ) as diet_goal from user u, " +
        "user_cat c,physhical_details pd where u.UID = ? and u.CATID = c.CAT_ID and u.status = ? and pd.UID =u.UID";
            params = [uid, DBConstants.userStatus.ACTIVE];

            connection(sql, params)
              .then((result) => {
                let token = tokenFactory.getToken({ type: "user", uid: uid });
                result['data'][0].token = token;
                result['data'][0]["password"] = "**********";

                result['data'][0]["profile_status"] = (result['data'][0]["height"] != null && result['data'][0]["weight"] != null)?'1':'0';
               result['data'][0]["goal_status"]  = result['data'][0]["user_goal"] > 0?'1':'0';
               result['data'][0]["diet_status"]  = result['data'][0]["diet_goal"] > 0?'1':'0';

                httpResponse.generateResponse(result['data'], 200, "Sign Up SuccessFully", res);
              }).catch((err) => {
                console.log(err);
                httpResponse.generateResponse(null, 500, status.USER_EXISTS, res);
              })
          }).catch((error) => {
            console.log(error);
            httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
          });
      }).catch((error) => {
        console.log(error);
        httpResponse.generateResponse(null, 500, status.USER_EXISTS, res);
      });
  }
});


module.exports = router;