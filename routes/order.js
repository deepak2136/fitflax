var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
var httpResponse = require("../util/HttpResponse");

const responseStatus = require("../util/constants");
const DBStatus = require("../util/database_constants");

const dateUtil = require("../util/dateUtil");

/**
 * this method return order history of user.
 * 
 * @param none
 * @returns order history of user
 */
router.get("/history", function (req, res, next) {
    let sql = `SELECT _order.* , _user.* , _payment_by.* , _payment_by.* , _offer.* 
                FROM \`order\` _order
                LEFT JOIN \`user\` _user ON _user.uid = _order.user
                LEFT JOIN payment_method _payment_by ON _payment_by.id = _order.payment_method
                LEFT JOIN plans p ON _order.plan_id = p.pid
                LEFT JOIN offers _offer ON _order.offer_id = _offer.barcode
                WHERE _order.user = ?
                ORDER BY _order.order_date DESC`;
    let params = [req.body.userTokenData["uid"]];
    let options = {
        sql: sql,
        nestTables: true
    }

    connection(options, params)
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, responseStatus.SUCCESS, res);
        }).catch((err) => {
            console.error(err);
            httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
        });
});

/**
 * this method add entry in DB after successful payment done by user.
 * only after you confitmed payment recieved.
 * 
 * @param {string} uid user id
 * @param {int} payment_method payment done through
 * @param {string} transecton_id
 * @param {String} reference_no [optional]
 * @param {string} summary [opstional]
 * @param {int} palan_id
 * @param {int} extra_charge [Optional]
 * @param {int} offer_id default send null
 * @param {int} discount
 */
router.post("/", (req, res, next) => {

    let user = req.body.uid;
    let payment_method = req.body.payment_method;
    let transecton_id = req.body.transection_id;
    let reference_no = req.body.reference_no;

    let summary = req.body.summary;
    let plan_id = req.body.plan_id;
    let extra_charge = req.body.extra_charge;
    let offer_id = req.body.offer_id;
    let discount = req.body.discount;

    let sql = 'select * from plans p where p.pid = ?';
    let params = [plan_id];

    if (!extra_charge)
        extra_charge = 0;
    if (!discount)
        discount = 0;

    if (!plan_id || !payment_method || !transecton_id || !user) {
        httpResponse.generateResponse(null, 403, responseStatus.INPUT_MISSING, res);
    } else {

        connection(sql, params)
            .then((result) => {
                if (result['data'].length > 0) {
                    var totalConsult = parseInt(result['data'][0]['CONSULT_COUNT']);
                    var totalAmmount = parseInt(result['data'][0]['FEES']);

                    if (offer_id !== undefined && offer_id !== null && offer_id) {
                        sql = 'select * from offers o where o.barcode = ? and o.status = ?';
                        params = [offer_id, 'active'];

                        connection(sql, params)
                            .then((result) => {

                                if (result['data'].length > 0) {
                                    let offerDiscount = result['data'][0]['DISCOUNT'] + "";
                                    if (!isNaN(offerDiscount)) {
                                        totalConsult += parseInt(offerDiscount);
                                    } else {
                                        let disRate = parseInt(discount.substring(0, offerDiscount.indexOf('%')));
                                        totalAmmount -= (totalAmmount * disRate / 100);
                                        totalAmmount = parseInt(totalAmmount);
                                    }

                                    placeOrder(user, totalAmmount, payment_method, transecton_id, reference_no,
                                        summary, plan_id, extra_charge, offer_id, totalConsult, discount, res);
                                } else {
                                    httpResponse.generateResponse(null, 403, responseStatus.INALID_OFFER, res);
                                }
                            }).catch((err) => {
                                console.error(err);
                                httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
                            });
                    } else {
                        placeOrder(user, totalAmmount, payment_method, transecton_id, reference_no,
                            summary, plan_id, extra_charge, offer_id, totalConsult, discount, res);
                    }
                } else {
                    httpResponse.generateResponse(null, 403, responseStatus.INVALID_PALN, res);
                }
            }).catch((err) => {
                console.error(err);
                httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
            });
    }
});

/**
 * this metod first add record in order table if record create successfully then it update
 * balance of user.
 * 
 * for add balance call mysql function. function def is listed below:
 * function addConsult(@param user_id, @param consult_count, @param plan_id) @return status
 * 201 = create new record in balance with consult count
 * 200 = update existing record.
 */
placeOrder = (user, ammount, payment_method, transecton_id, reference_no, summary, plan_id,
    extra_charge, offer_id, total_consult, discount, res) => {

    let finalAmmount = parseInt(ammount) + parseInt(extra_charge) - parseInt(discount);
    let currentDate = dateUtil.getCurrentDateTime();

    let sql = "insert into \`order\`(order_date, \`user\`, payment_method, ammount, transaction_no," +
        " reference_no, summary, plan_id, \`status\`, extra_charge, discount, offer_id) " +
        " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    let params = [currentDate, user, payment_method, finalAmmount, transecton_id, reference_no,
        summary, plan_id, DBStatus.status.COMPLETED, extra_charge, discount, offer_id];

    connection(sql, params)
        .then((result) => {
            httpResponse.generateResponse(null, 200, responseStatus.SUCCESS, res);

            sql = "select addConsult(?, ?, ?) as status_deepak FROM DUAL";
            params = [user, total_consult, plan_id];

            connection(sql, params)
                .then((result) => {
                    console.log("Consult count added into user balance successfully");
                }).catch((err) => {
                    console.log("error in adding consult to user");
                    console.error(err);
                });

            if (offer_id && offer_id !== undefined && offer_id !== null) {
                sql = 'SELECT updateCodeCounter(?, ?) AS status_depak FROM DUAL';
                params = [user, offer_id];

                connection(sql, params)
                    .then((result) => {
                        console.log("Code counter of offer is updated for user id : " + user);
                    }).catch((err) => {
                        console.log("Code counter of offer is updated failed for user id : " + user);
                        console.error(err);
                    });
            }
        }).catch((err) => {
            console.error(err);
            httpResponse.generateResponse(null, 406, responseStatus.ORDER_FAILED, res);
        });
}

module.exports = router;    