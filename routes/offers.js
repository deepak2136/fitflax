var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
const status = require("../util/constants");
var httpResponse = require("../util/HttpResponse");

/**
 * this method return all the offers.
 * 
 * @param none
 * @returns list of all offers.
 */
router.get("/list", function (req, res, next) {

  let sql = "select * from offers";
  let params = [];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * return the details of offer for given offer id.
 * 
 * @param {int} id offer id.
 * @returns offer deatails
 */
router.get("/get", function (req, res, next) {
  let barcode = req.query.id;
  let sql = "select * from offers where barcode =?";
  let params = [barcode];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
    });
});

/**
 * add new offer for given input data.
 * 
 * @param {int} id  offer id,
 * @param {date} date offer start date.
 * @param {string} status offer status.
 * @param {json} discount offer discounts rate in json format.
 */
router.post("/add", function (req, res, next) {

  let barcode = req.body.id;
  let date = req.body.date;
  let status = req.body.status;
  let discount = req.body.discount;

  if (!barcode || !date || !status || !discount) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "insert into offers values(?, ?, ?, ?)";
    let params = [barcode, date, status, discount];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 400, status.ALREADY_EXISTS, res);
      });
  }
});

/**
 * update offer details.
 *  
 * @param {int} id  offer id,
 * @param {date} date offer start date.
 * @param {string} status offer status.
 * @param {json} discount offer discounts rate in json format.
 */
router.put("/update", function (req, res, next) {

  let barcode = req.body.id;
  let date = req.body.date;
  let status = req.body.status;
  let discount = req.body.discount;

  if (!barcode || !date || !status || !discount) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "update offers o set o.date = ?, o.status = ?, o.discount = ? where o.barcode = ?";
    let params = [date, status, discount, barcode];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * delete offer for given offer id.
 * 
 * @param {int} id offer id.
 */
router.delete("/delete", function (req, res, next) {

  let barcode = req.body.id;
  if (!barcode) {
    httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
  } else {
    let sql = "delete from offers where barcode = ?";
    let params = [barcode];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

module.exports = router;