var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
const status = require("../util/constants");
var httpResponse = require("../util/HttpResponse");

/**
 * this method return all the goals.
 * 
 * @param none
 * @returns list of all goals.
 */
router.get("/list", function (req, res, next) {
    
    let sql = "select * from goal";
  
      // uid = req.body.userTokenData.uid;
 
      // sql = "select goal.*,user_goal.start_date,user_goal.end_date,user_goal.status from user_goal LEFT JOIN goal ON goal.GID = user_goal.GID where UID = "+uid;
      

  let params = [];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * return the details of goal for given goal id.
 * 
 * @param {int} id goal id.
 * @returns goal deatails
 */
router.get("/get", function (req, res, next) {

  let gid = req.query.id;
  if (!gid) {
    httpResponse.generateResponse(null, 400, status.INPUT_MISSING, res);
  } else {
    let sql = "select * from goal where gid = ?";
    let params = [gid];

    connection(sql, params)
      .then((result) => {
        httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
      }).catch((error) => {
        console.log(error);
        httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
      });
  }
});

/**
 * add new goal from given details.
 * 
 * @param {string} name goal name.
 * @param {string} desc description of goal.
 */
router.post("/add", function (req, res, next) {

  let name = req.body.name;
  let desc = req.body.desc;
  if (!name || !desc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "insert into goal(`NAME`, `DESC`) values(?, ?)";
    let params = [name, desc];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 400, status.ALREADY_EXISTS, res);
      });
  }
});

/**
 * add new goal from given details.
 * 
 * @param {int} id goal id.
 * @param {string} name goal name.
 * @param {string} desc description of goal.
 */
router.put("/update", function (req, res, next) {

  let gid = req.body.id;
  let name = req.body.name;
  let desc = req.body.desc;
  if (!gid || !name || !desc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "update goal g set g.name = ?, g.desc = ? where g.gid = ?";
    let params = [name, desc, gid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * delete goal for given goal id.
 * 
 * @param {int} id goal id.
 */
router.post("/delete", function (req, res, next) {

  let gid = req.body.id;
  if (!gid) {
    httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
  } else {
    let sql = "delete from goal where gid = ? ";
    let params = [gid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * get goal history of user. only admin have rights to see other user goal
 * history.
 * 
 * @param {int} id user id (admin only).
 */
router.get("/history", function (req, res, next) {

  let uid = req.body.userTokenData["uid"];;
  if (req.body.userTokenData["type"] == "admin" && req.query.id)
    uid = req.query.id;
  

  let sql = "SELECT g.*, u.* FROM user_goal u INNER JOIN goal g ON u.GID = g.GID WHERE u.UID = ?";
  let params = [uid];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

module.exports = router;