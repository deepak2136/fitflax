var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
const status = require("../util/constants");
var httpResponse = require("../util/HttpResponse");
// var base64ToImage = require('base64-to-image');



/**
 * this method return all the workouts.
 * 
 * @param none
 * @returns list of all workouts.
 */
router.get("/list", function (req, res, next) {

   let sql
     
   if (req.body.userTokenData.type == "admin") {

        sql = "select * from workout";

   } else {

        uid = req.body.userTokenData.uid;

        sql = "select goal.*,user_goal.start_date,user_goal.end_date,user_goal.status from user_goal LEFT JOIN goal ON goal.GID = user_goal.GID where UID = "+uid;


   }
  let params = [];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
    });
});

/**
 * return the details of workout for given workout id.
 * 
 * @param {int} id workout id.
 * @returns offer deatails
 */
router.get("/get", function (req, res, next) {

  let wid = req.query.id;
  let sql = "select * from workout where wid =?";
  let params = [wid];

  connection(sql, params)
    .then((result) => {
      httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
    }).catch((error) => {
      console.log(error);
      httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
    });
});

/**
 * add new workout for given input.
 * 
 * @param {string} name workout name.
 * @param {string} gif base64 string of gif image of workout.
 * @param {string} desc description of workout.
 */
router.post("/add", function (req, res, next) {

  let name = req.body.name;
  let gif = req.body.gif;
  let desc = req.body.desc;

  if (!name || !gif || !desc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {

    /*var base64Str = gif;
    var path ='../images/workout/';
    var optionalObj = {'fileName': 'imageFileName', 'type':'gif'};
    var imageInfo = base64ToImage(base64Str,path,optionalObj); 
    */
    let sql = "insert into workout(`NAME`, `GIF`, `DESC`) values(?, ?, ?)";
    let params = [name, gif, desc];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 500, status.ALREADY_EXISTSc, res);
      }).catch((error) => {
        console.error(error)
        httpResponse.generateResponse(null, 400, status.SERVER_ERROR, res);
      });
  }
});

/**
 * update workout for given input.
 * 
 * @param {int} id workout id.
 * @param {string} name workout name.
 * @param {string} gif base64 string of gif image of workout.
 * @param {string} desc description of workout.
 */
router.put("/update", function (req, res, next) {

  let wid = req.body.id;
  let name = req.body.name;
  let gif = req.body.gif;
  let desc = req.body.desc;

  if (!wid || !name || !gif || !desc) {
    httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
  } else {
    let sql = "update workout w set w.name = ?, w.gif = ?, w.desc = ? where w.wid = ?";
    let params = [name, gif, desc, wid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 201, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

/**
 * delete workout for given workout id.
 * 
 * @param {int} id workout id.
 */
router.post("/delete", function (req, res, next) {

  let wid = req.body.id;
  if (!wid) {
    httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
  } else {
    let sql = "delete from workout where wid = ?";
    let params = [wid];

    connection(sql, params)
      .then((result) => {
        if (result['data']["affectedRows"] > 0)
          httpResponse.generateResponse(null, 200, status.SUCCESS, res);
        else
          httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
      }).catch((error) => {
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      });
  }
});

module.exports = router;