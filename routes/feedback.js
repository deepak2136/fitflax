var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
var httpResponse = require("../util/HttpResponse");

const responseStatus = require("../util/constants");
const DBStatus = require("../util/database_constants");

const dateUtil = require("../util/dateUtil");

/**
 * give feedback or ask any query| questions.
 * 1 = question, 2 = feedback
 * 
 * @param {int} type feedback type (question or feedback)
 * @param {string} feedback 
 * @param {String} tags optional
 */
router.post("/", function (req, res, next) {

    let type = req.body.type;
    let uid = req.body.userTokenData["uid"];
    let feedback = req.body.feedback;
    let tags = req.body.tags;
    let dateTime = dateUtil.getCurrentDateTime();

    let sql = 'select count(*) as count from feedback where owner = ? and type = ?';
    let params = [uid, DBStatus.feedback_type.FEEDBACK];

    connection(sql, params)
        .then((result) => {
            let count = parseInt(result['data'][0].count);
            if (count == 0) {
                sql = 'insert into feedback(type, date_time, owner, feedback, tags) values(?, ?, ?, ?, ?)';
                params = [type, dateTime, uid, feedback, tags];

                connection(sql, params)
                    .then((result) => {
                        httpResponse.generateResponse({ id: result['data'].insertId }, 200, responseStatus.SUCCESS, res);
                    }).catch((err) => {
                        console.error(err);
                        httpResponse.generateResponse(null, 403, responseStatus.INVALID_TOKEN, res);
                    });
            } else {
                httpResponse.generateResponse(null, 405, responseStatus.FEEDBACK_GIVEN, res);
            }
        }).catch((err) => {
            console.error(err);
            httpResponse.generateResponse(null, 500, responseStatus.SERVER_ERROR, res);
        });
});


module.exports = router;    