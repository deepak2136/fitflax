var express = require("express");
var router = express.Router();

const connection = require("../util/MySqlMaster");
var httpResponse = require("../util/HttpResponse");

const status = require("../util/constants");

/**
 * return all availbale payments method to pay ammount. option can be offline as well
 * as offline.
 */
router.get("/list", function (req, res, next) {
    let sql = "select * from payment_method";
    connection(sql, [])
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
        }).catch((error) => {
            console.log(error);
            httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
        });
});

/**
 * get details of any specific payment_method
 * 
 * @param {int} id payment_method id
 */
router.get("/get", function (req, res, next) {

    let id = req.query.id;
    let sql = "select pm.* from payment_method pm where pm.id = ?";

    connection(sql, [id])
        .then((result) => {
            httpResponse.generateResponse(result['data'], 200, status.SUCCESS, res);
        }).catch((error) => {
            console.log(error);
            httpResponse.generateResponse(null, 404, status.RECORD_NOT_FOUND, res);
        });
});

/**
 * add new payment method to availble list of payment.
 * 
 * @param {string} name payment method name
 * @param {string} desc description
 */
router.post("/add", function (req, res, next) {
    let name = req.body.name;
    let desc = req.body.desc;

    if (!name) {
        httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
    } else {
        let sql = "insert into payment_method(method, description) values(?, ?)";
        let params = [name, desc];

        connection(sql, params)
            .then((result) => {
                if (result['data']["affectedRows"] > 0)
                    httpResponse.generateResponse(null, 201, status.SUCCESS, res);
                else
                    httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            }).catch((error) => {
                console.log(error)
                httpResponse.generateResponse(null, 400, status.ALREADY_EXISTS, res);
            });
    }
});

/**
 * update exisiting patment method details.
 * 
 * @param {string} name payment method name
 * @param {string} desc description
 * @param {int} id paymetn_id
 */
router.put("/update", function (req, res, next) {

    let name = req.body.name;
    let desc = req.body.desc;
    let id = req.body.id;

    if (!id) {
        httpResponse.generateResponse(null, 401, status.INPUT_MISSING, res);
    } else {

        let sql = "update payment_method pm set pm.method = ? , pm.description = ? where pm.id = ?";
        let params = [name, desc, id];

        connection(sql, params)
            .then((result) => {
                if (result['data']["affectedRows"] > 0)
                    httpResponse.generateResponse(null, 201, status.UPDATE_SUCCESS, res);
                else
                    httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
            }).catch((error) => {
                console.log(error)
                httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });
    }
});

/**
 * delete payment method from availbale list.
 * 
 * @param {int} id payment_method id
 */
router.delete("/delete", function (req, res, next) {
    let id = req.body.id;
    if (!id) {
        httpResponse.generateResponse(null, 404, status.NPUT_MISSING, res);
    } else {
        let sql = "delete from payment_method where id = ?";
        let params = [id];

        connection(sql, params)
            .then((result) => {
                if (result['data']["affectedRows"] > 0)
                    httpResponse.generateResponse(null, 200, status.SUCCESS, res);
                else
                    httpResponse.generateResponse(null, 400, status.NOT_EXISTS, res);
            }).catch((error) => {
                httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
            });
    }
});

module.exports = router;   