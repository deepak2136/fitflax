const transporter = require("./emailConf");

/**
 * constans used for mail.
 */
const companyEmail = 'ffaproject7@gmail.com';
const subjectForgotPassword = 'Forgot Password';

/**
 * this method http reponse to send new code to user. this function only return html templet.
 * 
 * @param {string} userId user Name or user ID
 * @param {string} password new system genrated password;
 */
getHtmlResponse = (userId, password) => {
    return "<html><head><title>" + subjectForgotPassword + "</title></head><body>Dear " + userId +
        ", <br><br>As per your request your password is reset. the new password for your " +
        "account is<b> " + password + " </b>. now you can login with given password. We " +
        "adviced you to change your password after login.<br> <br>Please do not share password " +
        "with anyone. <br><br>Thanking you,<br>Fitflax support group.</body></html>"
};

/**
 * this method is send email to user for new system genrated password due to user choose forgot
 * password option.
 * 
 * @param {string} to sending to email
 * @param {string} password new system generated password
 * @param {string} userId user id
 */
module.exports.sendmail = (toEmail, password, userId) => {
    let mailOption = {
        from: companyEmail,
        to: toEmail,
        subject: subjectForgotPassword,
        html: getHtmlResponse(userId, password)
    };

    transporter.sendMail(mailOption, (err, info) => {
        if (err) {
            console.error(err);
        } else {
            console.log('email send successfully');
            console.log(info);
        }
    });
}

/**
 * this method is send email to user for mysql database failuer from backend;
 * 
 * @param {error} err error logs to debug the cousion of error.
 */
module.exports.sendmailMySqlFail = (err) => {
    let mailOption = {
        from: companyEmail,
        to: 'deepakprajapati589@gmail.com',
        subject: 'MySql Database failure.',
        text: 'MySql Database connection failed. \n\n\n see error logs \n\n ' + err
    };

    transporter.sendMail(mailOption, (err, info) => {
        if (err) {
            console.error(err);
        } else {
            console.log('email send successfully');
            console.log(info);
        }
    });
}