const nodemailer = require('nodemailer');

/**
 * email configuration file. set email password and smtp and you are good to go.
 */
module.exports = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
        user: 'ffaproject7@gmail.com',
        pass: 'ffaproject123'
    }
});