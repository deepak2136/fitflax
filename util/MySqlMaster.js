const conn = require("../MySqlConf");
const status = require("./constants");
const email = require("./email");

/**
 * use conn.query method. it consists of fours methods.
 *      1.   create connection
 *      2.   execute query
 *      3.   release connection
 * 
 * release happens immidiate when response aarive.
 */


/**
 * @argument sql sql query
 * @argument params parameters set into sql query.
 * @argument start start record number
 * @argument limit no of records you need as a result (default = 100)
 * 
 *  this is a master function which execute query and rerurn response | error.
 *  it return promise so to handel function use then and catch block.
 * 
 * @returns result if succcess
 * @returns error if exception encounter.
 */
module.exports = function (sql, params, start, limit) {

    return new Promise(function (resolve, reject) {
        try {
            /**
             * setting up limit and offset of MySql query. if start values is passed
             * but limit is not given then default limit set to 100 records.
             */
            if (start !== undefined) {
                sql = sql.replace(/;/g, '');
                sql += " limit " + start;

                if (limit === undefined || typeof (limit) === 'undefined') {
                    limit = 100;
                }
                sql += " ," + limit;
                console.log(sql)
            }
            conn.query(sql, params, function (error, result, fields) {
                if (error) {
                    console.log("Excception in sql excution.");
                    reject(error);
                } else {
                    //console.log("query executed successfully.");
                    let response = {
                        "fields": fields,
                        "data": result
                    };
                    resolve(response);
                }
            });
        } catch (err) {
            console.log(status.MYSQL_NOT_WORKING);
            console.error(err);
            email.sendmailMySqlFail(err);
            reject(err);
        }
    });
}

