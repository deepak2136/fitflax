/**
 * this uction is utlity to generate response.
 * @param {any} data 
 * @param {number} status 
 * @param {string} message 
 * @param {any} res response object
 */
module.exports.generateResponse = function (data, status, message, res) {
    var response = [
        {
            "data": data,
            "status": status,
            "message": message
        }
    ];

    res.status(status);
    res.json(response);
};