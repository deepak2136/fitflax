var dateTime = require('node-datetime');
const dateTimeFormat = 'Y/m/d H:I:M:S p';
const dateFormat = 'Y/m/d';
const timeFormat = 'H:I:M:S p';

module.exports = {
    /**
     * get current date in this format. 2018/03/23.
     */
    getCurrentDate: function () {
        let date = dateTime.create(new Date());
        return date.format(dateFormat);
    },

    /**
     * get current time in this format.12:24:47 AM
     */
    getCurrentTime: function () {
        let date = dateTime.create(new Date());
        return date.format(timeFormat);
    },

    /**
     * get current dateTime in this format. 2018/03/27 12:24:47 AM
     */
    getCurrentDateTime: function () {
        let date = dateTime.create(new Date());
        return date.format(dateTimeFormat);
    },

    /**
     * comapre two dates and return integer.
     * 
     * @param {string} date1 in format '2018/03/27 12:24:47 AM'
     * @param {string} date2 in format '2018/03/27 12:24:47 AM'
     * 
     * @returns {1} first one is larger
     * @returns {0} if both same
     * @returns {-1} first one is smaller
     */
    compareDate: function (date1, date2) {
        let d1 = new Date(date1);
        let d2 = new Date(date2);

        if (d1.getTime() === d2.getTime())
            return 0;
        else
            return d1 >= d2 ? 1 : -1;
    }
};