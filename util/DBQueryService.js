const connection = require("./MySqlMaster");
const httpResponse = require("./HttpResponse");
const status = require("./constants");
const emailService = require("./email");

module.exports = async (sql, params, res) => {
    try {
        let responseData = await connection(sql, params);
        return responseData;
      } catch (err) {
        console.log(err);
        emailService.sendmailMySqlFail(err);
        httpResponse.generateResponse(null, 500, status.SERVER_ERROR, res);
      }
};