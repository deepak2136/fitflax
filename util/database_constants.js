module.exports = {
    /* store user types */
    userType: {
        ADMIN: 'admin',
        USER: 'user',
        TRAINEE: 'trainee',
        CASHIER: 'cashier',
        PAYMENT_GETWAY: 'payment_getway'
    },

    /*
    * store user_cat database values.
    */
    userTypeId: {
        ADMIN: 1,
        USER: 2,
        TRAINEE: 3,
        CAHSIER: 4,
        PAYMENT_GETWAY: 5
    },

    /* store diffrent status for user account */
    userStatus: {
        ACTIVE: 'active',
        SUSPENEDED: 'suspended',
        BLOCKED: 'blocked',
        INACTIVE: 'in active'
    },

    /* consult type */
    consultType: {
        NUTERATION: 'Nuteration',
        GYM: 'Gym'
    },

    /* diet status */
    dietStatus: {
        REQUESTED: 'requested',
        POSPONED: 'posponed',
        COMPLETE: 'completed',
        ASSIGNED: 'assigned'
    },

    /* duretion details */
    duretion: {
        M1: '1 month',
        M2: '2 months',
        M3: '3 months',
        M4: '4 months',
        M6: '6 months',
        M12: '12 months',
        Y1: '1 year',
        W1: '1 week',
        W2: '2 weeks',
        W3: '3 weeks',
        D1: '1 Day',
        D3: '3 days',
        D7: '7 days',
        D10: '10 days'
    },

    /* feedback types */
    feedback_type: {
        QUESTION: 1,
        FEEDBACK: 2
    },

    goal_status: {
        REQUEST: 'requested',
        ACTIVE: 'active',
        COMPLETE: 'complete',
        BLOCKED: 'blocked'
    },

    /* status for order */
    order: {
        ORDERED: 'odered',
        CANCELLED: 'cancelled',
        FAILED: 'failed'
    },

    status: {
        REQUESTED: 1,
        ACTIVE: 2,
        CANCELLED: 3,
        REJECTED: 4,
        BLOCKED: 5,
        COMPLETED: 6,
        PANDING: 7,
        INPROGRESS: 8,
        RISTRCTED: 9
    }
}