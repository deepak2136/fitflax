CREATE DATABASE  IF NOT EXISTS `sql3229022` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sql3229022`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: sql3.freesqldatabase.com    Database: sql3229022
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assign_user`
--

DROP TABLE IF EXISTS `assign_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assign_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TID` int(11) NOT NULL,
  `UID` varchar(45) NOT NULL,
  `GID` int(11) NOT NULL,
  `DESC` tinytext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `UID` (`UID`),
  KEY `TID` (`TID`),
  KEY `GID` (`GID`),
  CONSTRAINT `assign_user_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `user` (`UID`),
  CONSTRAINT `assign_user_ibfk_2` FOREIGN KEY (`TID`) REFERENCES `trainee` (`TID`),
  CONSTRAINT `assign_user_ibfk_3` FOREIGN KEY (`GID`) REFERENCES `goal` (`GID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assign_user`
--

LOCK TABLES `assign_user` WRITE;
/*!40000 ALTER TABLE `assign_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `assign_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `balance`
--

DROP TABLE IF EXISTS `balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `balance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UID` varchar(45) NOT NULL,
  `COUNT` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `TYPE` (`TYPE`),
  CONSTRAINT `balance_ibfk_1` FOREIGN KEY (`TYPE`) REFERENCES `consult_master` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `balance`
--

LOCK TABLES `balance` WRITE;
/*!40000 ALTER TABLE `balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `code_counter`
--

DROP TABLE IF EXISTS `code_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_counter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` varchar(45) NOT NULL,
  `BARCODE` varchar(45) NOT NULL,
  `COUNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `id_UNIQUE` (`ID`),
  KEY `UID` (`UID`),
  KEY `BARCODE` (`BARCODE`),
  CONSTRAINT `code_counter_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `user` (`UID`),
  CONSTRAINT `code_counter_ibfk_2` FOREIGN KEY (`BARCODE`) REFERENCES `offers` (`BARCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `code_counter`
--

LOCK TABLES `code_counter` WRITE;
/*!40000 ALTER TABLE `code_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `code_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consult_master`
--

DROP TABLE IF EXISTS `consult_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consult_master` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `DESC` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consult_master`
--

LOCK TABLES `consult_master` WRITE;
/*!40000 ALTER TABLE `consult_master` DISABLE KEYS */;
INSERT INTO `consult_master` VALUES (1,'Nuterist',NULL),(2,'Jimiest',NULL);
/*!40000 ALTER TABLE `consult_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diet`
--

DROP TABLE IF EXISTS `diet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diet` (
  `DID` int(11) NOT NULL AUTO_INCREMENT,
  `ASID` int(11) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `REQUEST_DATE` varchar(45) DEFAULT NULL,
  `PHOTO` varchar(45) DEFAULT NULL,
  `WORKOUT` mediumtext,
  `ASSIGN_DATE` varchar(45) DEFAULT NULL,
  `DURETION` varchar(45) DEFAULT NULL,
  `COUNTER` int(11) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `HEIGHT` varchar(45) DEFAULT NULL,
  `WEIGHT` varchar(45) DEFAULT NULL,
  `REMARK` tinytext,
  `DIET` mediumtext,
  PRIMARY KEY (`DID`),
  UNIQUE KEY `did_UNIQUE` (`DID`),
  KEY `ASID` (`ASID`),
  KEY `TYPE` (`TYPE`),
  CONSTRAINT `diet_ibfk_1` FOREIGN KEY (`ASID`) REFERENCES `assign_user` (`ID`),
  CONSTRAINT `diet_ibfk_2` FOREIGN KEY (`TYPE`) REFERENCES `consult_master` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diet`
--

LOCK TABLES `diet` WRITE;
/*!40000 ALTER TABLE `diet` DISABLE KEYS */;
/*!40000 ALTER TABLE `diet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goal`
--

DROP TABLE IF EXISTS `goal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goal` (
  `GID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(65) NOT NULL,
  `DESC` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`GID`),
  UNIQUE KEY `GID_UNIQUE` (`GID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goal`
--

LOCK TABLES `goal` WRITE;
/*!40000 ALTER TABLE `goal` DISABLE KEYS */;
INSERT INTO `goal` VALUES (1,'Workout','Details for workout.'),(2,'Body Buildin','Put some details for category.');
/*!40000 ALTER TABLE `goal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image_store`
--

DROP TABLE IF EXISTS `image_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_store` (
  `image_id` char(45) CHARACTER SET utf8 NOT NULL,
  `image_data` text NOT NULL,
  `owner_id` varchar(45) NOT NULL,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `image_id_UNIQUE` (`image_id`),
  KEY `o_idx` (`owner_id`),
  CONSTRAINT `o` FOREIGN KEY (`owner_id`) REFERENCES `user` (`UID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image_store`
--

LOCK TABLES `image_store` WRITE;
/*!40000 ALTER TABLE `image_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `image_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offers` (
  `BARCODE` varchar(40) NOT NULL,
  `DATE` varchar(45) NOT NULL,
  `STATUS` varchar(45) NOT NULL,
  `DISCOUNT` varchar(45) NOT NULL,
  PRIMARY KEY (`BARCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offers`
--

LOCK TABLES `offers` WRITE;
/*!40000 ALTER TABLE `offers` DISABLE KEYS */;
INSERT INTO `offers` VALUES ('ABC123','2017/02/11','active','10'),('XYZ12','2016/12/25','not-active','12');
/*!40000 ALTER TABLE `offers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physhical_details`
--

DROP TABLE IF EXISTS `physhical_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physhical_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` varchar(45) NOT NULL,
  `HEIGHT` varchar(45) DEFAULT NULL,
  `WEIGHT` varchar(45) DEFAULT NULL,
  `DISABILITY` varchar(256) DEFAULT NULL,
  `REMARK` varchar(256) DEFAULT NULL,
  `CAT` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `id_UNIQUE` (`ID`),
  KEY `UID` (`UID`),
  CONSTRAINT `physhical_details_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `user` (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physhical_details`
--

LOCK TABLES `physhical_details` WRITE;
/*!40000 ALTER TABLE `physhical_details` DISABLE KEYS */;
INSERT INTO `physhical_details` VALUES (2,'dipak','172','72','No disablility','No remark','Vej Non-jain'),(3,'user','172','66','none','low suger patient','non jain | veg'),(4,'vatsal',NULL,NULL,NULL,NULL,NULL),(5,'abcd',NULL,NULL,NULL,NULL,NULL),(6,'kishan',NULL,NULL,NULL,NULL,NULL),(7,'abcd123',NULL,NULL,NULL,NULL,NULL),(8,'abcd12345',NULL,NULL,NULL,NULL,NULL),(9,'12345678',NULL,NULL,NULL,NULL,NULL),(10,'123',NULL,NULL,NULL,NULL,NULL),(11,'1245','172','66','none','low suger patient','non jain | veg'),(12,'12456565',NULL,NULL,NULL,NULL,NULL),(13,'12456565scc',NULL,NULL,NULL,NULL,NULL),(14,'1458','wbbs','nsbs','zhhz','bzbz','bzzb');
/*!40000 ALTER TABLE `physhical_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plans`
--

DROP TABLE IF EXISTS `plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plans` (
  `PID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `DETAILS` tinytext,
  `FEES` float NOT NULL,
  `DURETION` varchar(45) NOT NULL,
  `CONSULT_COUNT` int(11) NOT NULL,
  `TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`PID`),
  UNIQUE KEY `PID_UNIQUE` (`PID`),
  KEY `TYPE` (`TYPE`),
  CONSTRAINT `plans_ibfk_1` FOREIGN KEY (`TYPE`) REFERENCES `consult_master` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plans`
--

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` VALUES (6,'BAZZAR SELL','put some details',1800,'3 month',13,1),(7,'BAZZAR SELL','put some details',1800,'3 month',13,1);
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainee`
--

DROP TABLE IF EXISTS `trainee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainee` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` varchar(45) NOT NULL,
  `CONTACT` varchar(45) NOT NULL,
  `AVAILBALE_TIME_START` varchar(45) NOT NULL,
  `AVAILABLE_TIME_END` varchar(45) NOT NULL,
  `GID` int(11) NOT NULL,
  `ACHIVEMENTS` varchar(256) DEFAULT NULL,
  `TYPE` int(11) NOT NULL,
  PRIMARY KEY (`TID`),
  KEY `UID` (`UID`),
  KEY `GID` (`GID`),
  KEY `TYPE` (`TYPE`),
  CONSTRAINT `trainee_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `user` (`UID`),
  CONSTRAINT `trainee_ibfk_2` FOREIGN KEY (`GID`) REFERENCES `goal` (`GID`),
  CONSTRAINT `trainee_ibfk_3` FOREIGN KEY (`TYPE`) REFERENCES `consult_master` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainee`
--

LOCK TABLES `trainee` WRITE;
/*!40000 ALTER TABLE `trainee` DISABLE KEYS */;
INSERT INTO `trainee` VALUES (1,'vatsal','1231231','09:20 AM','07:30 PM',2,'achivment 1\r\nachivment 2',1);
/*!40000 ALTER TABLE `trainee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `UID` varchar(20) NOT NULL,
  `NAME` varchar(45) NOT NULL,
  `EMAIL` varchar(65) NOT NULL,
  `ADDRESS` varchar(300) NOT NULL,
  `PHOTO` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(256) NOT NULL,
  `CONTACT` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) NOT NULL,
  `JOIN_DATE` varchar(45) DEFAULT NULL,
  `CATID` int(11) NOT NULL,
  `BIRTHDATE` varchar(45) DEFAULT NULL,
  `GENDER` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`UID`,`ADDRESS`),
  UNIQUE KEY `UID_UNIQUE` (`UID`),
  KEY `CATID` (`CATID`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`CATID`) REFERENCES `user_cat` (`CAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('123','vaikunj gandhi','vaikunj68@jhjnjnjnj.com','',NULL,'$2a$10$873qFzzh.5ZlNlodWR25Mur7jOyAuzBWcBzqwhyd1jTRBXfimWkN6','98988907585','active',NULL,2,NULL,'Male'),('12345678','vakunjnscbnbzchjhhbjnnmbnm','vaikunj6888tyfggf1111@jhjnjnjnj.com','',NULL,'$2a$10$ORFtpXfFEXWYQLismAvOAeJRdj/N/.ArrpgS0BayDDinMf/XD80O2','123444569','active',NULL,2,NULL,'Male'),('1245','user test','vaikunj68@gmail.com','',NULL,'123456','99887766','active',NULL,2,'03/11/1995','M'),('12456565','vaikunj gandhi54545','vaikunj6845@gmail.com','',NULL,'$2a$10$iJOo32PkJ224En02uW1EFOWO2xJwu1ofH4BI5hkeMhSXJ0IQIWE22','12345','active',NULL,2,NULL,'Femal'),('12456565scc','vaikunj gandhi54545','vaikunj6845vsvf@gmail.com','',NULL,'$2a$10$SZXGQczft0KyUJ7KT/ZIoOxnK.F12jqxOOjHLL4BrgA4a9VfrzXJG','12345','active',NULL,2,NULL,'Femal'),('1458','vaikunj gandhi54545ncs nz','vaikunj@gmail.com','',NULL,'$2a$10$Bb5gk5zpRvhEN8ujnYU3yeBvQSvF263EMnToKTbWWaLnphosM//XO','12345','active',NULL,2,NULL,'Femal'),('abcd','don\'t ask','email1','',NULL,'$2a$10$SKWJTrAiKwwk/K0YQMFvjesAB/QHmE4wdn2bL3..UJ5s8zt0.YE9O',NULL,'active',NULL,2,NULL,NULL),('abcd123','don\'t askbas','email1hbasnnas','',NULL,'$2a$10$yA2zAMa0bFAEZSFJh5J.Bezo1DXG67aCyENkaR8JyGhBCdAP6e3/S',NULL,'active',NULL,2,NULL,NULL),('abcd12345','vaikunj','vaikunj68@gmail.com','',NULL,'$2a$10$NoCZ3xtdSP9VkocsmJM0D.mTEdeeky7vGHcuuevA25G19kinnu9dO',NULL,'active',NULL,2,NULL,NULL),('dipak','test','dipak@test.com','',NULL,'$2a$10$gQ28nV4sLCQ2GFAIYb5YpOpA7aCkIVR2CzkID86vkB.ru85Z9oMhC','12344321','active',NULL,1,NULL,'G'),('kishan','kishan soni','kishan@gmail.com','',NULL,'$2a$10$oCCueICYK/52xLxzp3jOpeHFTMcCGfUeRzlCZjs2dT2n/G1DCIlvq',NULL,'active',NULL,2,NULL,NULL),('user','user test','user@test.com','','user','$2a$10$WsecmbVT3xN5VBiA4Ujgs.auL/A.sFgxmSUi7q8izLhq3XCpxRNFK','99887766','active',NULL,2,'03/11/1995','M'),('vatsal','test','vatsal@test.com','',NULL,'$2a$10$9rXNQMHr5vWRO.MwIQd7/u6JlEWnIh5MIU4IRvslf3yyPx12cm0FC','12344321','active',NULL,3,NULL,'M');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_cat`
--

DROP TABLE IF EXISTS `user_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_cat` (
  `CAT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CAT_ID`),
  UNIQUE KEY `CAT_ID_UNIQUE` (`CAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_cat`
--

LOCK TABLES `user_cat` WRITE;
/*!40000 ALTER TABLE `user_cat` DISABLE KEYS */;
INSERT INTO `user_cat` VALUES (1,'admin'),(2,'user'),(3,'trainee');
/*!40000 ALTER TABLE `user_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_goal`
--

DROP TABLE IF EXISTS `user_goal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_goal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UID` varchar(45) NOT NULL,
  `GID` int(11) NOT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `START_DATE` varchar(45) DEFAULT NULL,
  `END_DATE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `GID` (`GID`,`UID`),
  KEY `UID` (`UID`),
  CONSTRAINT `user_goal_ibfk_1` FOREIGN KEY (`UID`) REFERENCES `user` (`UID`),
  CONSTRAINT `user_goal_ibfk_2` FOREIGN KEY (`GID`) REFERENCES `goal` (`GID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_goal`
--

LOCK TABLES `user_goal` WRITE;
/*!40000 ALTER TABLE `user_goal` DISABLE KEYS */;
INSERT INTO `user_goal` VALUES (1,'dipak',1,'active','2018/03/11',NULL),(2,'dipak',2,'complete','2017/01/01','2017/09/30'),(3,'user',1,'request','2018-3-16 02:45:37',NULL);
/*!40000 ALTER TABLE `user_goal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout`
--

DROP TABLE IF EXISTS `workout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout` (
  `WID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `GIF` mediumtext,
  `DESC` tinytext,
  PRIMARY KEY (`WID`),
  UNIQUE KEY `WID_UNIQUE` (`WID`)
) ENGINE=InnoDB AUTO_INCREMENT=706 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout`
--

LOCK TABLES `workout` WRITE;
/*!40000 ALTER TABLE `workout` DISABLE KEYS */;
INSERT INTO `workout` VALUES (1,'back flip',NULL,'put some details.'),(2,'deeps',NULL,'some details.'),(702,'name','value','data'),(703,'name','VGVzdCBiYXNlNjQ=','description'),(704,'name1','VGVzdCBiYXNlNjQ=','description'),(705,'name','value','data');
/*!40000 ALTER TABLE `workout` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-26 23:18:26
